﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class FormulaFunction
    {
        private static readonly char[] BinaryOperations = { '+', '*', '>' };
        private static bool IsFormula(string formula, bool firstTime)
        {
            int formulaLength = formula.Length;
            for (int i = 0; i < formulaLength; i++)
            {
                if (formula[i] == '(')
                {
                    //Ищем конец скобки
                    int BracketBalance = 1;
                    int EndOfBracket = 0;
                    for (int j = i + 1; j < formulaLength; j++)
                    {
                        if (formula[j] == '(')
                        {
                            BracketBalance++;
                        }
                        else if (formula[j] == ')')
                        {
                            BracketBalance--;
                            if (BracketBalance == 0)
                            {
                                EndOfBracket = j;
                                break;
                            }
                        }
                    }

                    //Рекурсивно запускаем метод чтобы найти следующее выражение 
                    if (EndOfBracket != 0)
                    {
                        string newFormula = formula.Substring(i + 1, EndOfBracket - i - 1);
                        if (IsFormula(newFormula, false))
                        {
                            formula = formula.Remove(i, newFormula.Length + 2);
                            formula = formula.Insert(i, "a");
                            formulaLength = formula.Length;
                        }
                        else
                            return false;
                    }
                }

            }
            if ((formulaLength == 3) && (!firstTime))
            {
                if ((char.IsLetter(formula[0])) && (BinaryOperations.Contains(formula[1])) && (char.IsLetter(formula[2])))
                    return true;
            }

            else if ((formulaLength == 1) && (firstTime))
            {
                if (char.IsLetter(formula[0]))
                    return true;
            }
            return false;
        }

        private static string RemoveNegations(string formula)
        {
            for (int i = 0; i < formula.Length; i++)
            {
                if (formula[i] == '~')
                    if (i + 1 < formula.Length)
                    {
                        if (!BinaryOperations.Contains(formula[i + 1]))
                            formula = formula.Remove(i, 1);
                        i--;
                    }
            }
            return formula;
        }

        public static bool Check(string f)
        {
            f = RemoveNegations(f);
            if (f.Contains("~"))
                return false;
            if (IsFormula(f, true))
                return true;
            return false;
        }

        private static int CountFormula(string formula, Dictionary<char, int> values)
        {
            for (int i = 0; i < formula.Length; i++)
            {
                if (values.ContainsKey(formula[i]))
                {
                    formula = formula.Replace(formula[i], char.Parse(values[formula[i]].ToString()));
                }
            }
            for (int i = 0; i < formula.Length; i++)
            {
                if (formula[i] == '(')
                {
                    //Ищем конец скобки
                    int BracketBalance = 1;
                    int EndOfBracket = 0;
                    for (int j = i + 1; j < formula.Length; j++)
                    {
                        if (formula[j] == '(')
                        {
                            BracketBalance++;
                        }
                        else if (formula[j] == ')')
                        {
                            BracketBalance--;
                            if (BracketBalance == 0)
                            {
                                EndOfBracket = j;
                                break;
                            }
                        }
                    }

                    //Рекурсивно запускаем метод чтобы найти следующее выражение 
                    if (EndOfBracket != 0)
                    {
                        string newFormula = formula.Substring(i + 1, EndOfBracket - i - 1);
                        int formulaValue = CountFormula(newFormula, values);
                        formula = formula.Remove(i, newFormula.Length + 2);
                        formula = formula.Insert(i, formulaValue.ToString());

                    }
                }
            }
            if (formula.Length > 1)
            {
                int NegationA = 0;
                int NegationB = 0;
                int a = 0;
                int b = 0;
                bool foundOperator = false;
                char _operator = '+';
                for (int k = 0; k < formula.Length; k++)
                {
                    if ((formula[k] == '~') && (!foundOperator))
                    {
                        NegationA++;
                    }
                    else if ((char.IsDigit(formula[k])) && (!foundOperator))
                    {
                        a = (int.Parse(formula[k].ToString()) + NegationA) % 2;
                    }
                    else if (BinaryOperations.Contains(formula[k]))
                    {
                        foundOperator = true;
                        _operator = formula[k];
                    }
                    else if ((formula[k] == '~') && (foundOperator))
                    {
                        NegationB++;
                    }
                    else if ((char.IsDigit(formula[k])) && (foundOperator))
                    {
                        b = (int.Parse(formula[k].ToString()) + NegationB) % 2;
                    }
                }
                formula = RemoveNegations(formula);
                switch (_operator)
                {
                    case '+':
                        {
                            if (a + b == 0)
                                return 0;
                            else
                                return 1;
                        }
                    case '*':
                        {
                            return a * b;
                        }
                    case '>':
                        {
                            if ((((a + 1) % 2) + b) == 0)
                                return 0;
                            else
                                return 1;
                        }
                }
            }
            return int.Parse(formula[0].ToString());
        }

        public static string BuildDNF(string formula)
        {
            List<char> variables = new List<char>();
            StringBuilder DNF = new StringBuilder();
            foreach(char c in formula)
            {
                if(char.IsLetter(c))
                {
                    if (!variables.Contains(c))
                        variables.Add(c);
                }
            }
            int[] values = new int[variables.Count];
            for(int i = 0;i<variables.Count;i++)
            {
                values[i] = 0;
            }
            for(int i =0;i<Math.Pow(2,variables.Count);i++)
            {
                Dictionary<char, int> valuesAndVariables = new Dictionary<char, int>();
                for(int j = 0;j<variables.Count;j++)
                {
                    valuesAndVariables.Add(variables[j], values[j]);
                }
                if(CountFormula(formula,valuesAndVariables)==1)
                {
                    DNF.Append("(");
                    for(int j =0;j<variables.Count;j++)
                    {
                        if (values[j] == 0)
                            DNF.Append($"~{variables[j]}");
                        else
                            DNF.Append($"{variables[j]}");
                        DNF.Append("*");
                    }
                    DNF.Remove(DNF.Length - 1,1);
                    DNF.Append(")+");
                }
                if (values[values.Length - 1] == 0)
                    values[values.Length - 1] = 1;
                else
                {
                    values[values.Length - 1] = 0;
                    for(int j = values.Length-2;j>=0;j--)
                    {
                        if (values[j] == 0)
                        {
                            values[j] = 1;
                            break;
                        }
                        else
                            values[j] = 0;
                    }
                }
            }
            DNF.Remove(DNF.Length - 1,1);
            if(DNF.Length==3)
            {
                DNF.Remove(0, 1);
                DNF.Remove(1, 1);
            }
            return DNF.ToString();
        }

        static public string BuildKNF(string formula)
        {
            List<char> variables = new List<char>();
            StringBuilder KNF = new StringBuilder();
            foreach(char c in formula)
            {
                if(char.IsLetter(c))
                {
                    if (!variables.Contains(c))
                        variables.Add(c);
                }
            }
            int[] values = new int[variables.Count];
            for(int i = 0;i<values.Length;i++)
            {
                values[i] = 0;
            }

            for(int i =0;i<Math.Pow(2,values.Length);i++)
            {
                Dictionary<char, int> valuesAndVariables = new Dictionary<char, int>();
                for(int j = 0;j<variables.Count;j++)
                {
                    valuesAndVariables.Add(variables[j], values[j]);
                }
                if(CountFormula(formula,valuesAndVariables)==0)
                {
                    KNF.Append("(");
                    for(int j =0;j<variables.Count;j++)
                    {
                        if (values[j] == 1)
                            KNF.Append($"~{variables[j]}");
                        else
                            KNF.Append($"{variables[j]}");
                        KNF.Append("+");
                    }
                    KNF.Remove(KNF.Length - 1,1);
                    KNF.Append(")*");
                }
                if (values[values.Length - 1] == 0)
                    values[values.Length - 1] = 1;
                else
                {
                    values[values.Length - 1] = 0;
                    for(int j = values.Length-2;j>=0;j--)
                    {
                        if (values[j] == 0)
                        {
                            values[j] = 1;
                            break;
                        }
                        else
                            values[j] = 0;
                    }
                }
            }
            KNF.Remove(KNF.Length - 1,1);
            if(KNF.Length==3)
            {
                KNF.Remove(0, 1);
                KNF.Remove(1, 1);
            }
            return KNF.ToString();
        }

        static public bool CheckIfEqual(string formula1,string formula2)
        {
            List<char> variables1 = new List<char>();
            List<char> variables2 = new List<char>();
            foreach(char c in formula1)
            {
                if(char.IsLetter(c))
                {
                    if (!variables1.Contains(c))
                        variables1.Add(c);
                }
            }
            foreach(char c in formula2)
            {
                if(char.IsLetter(c))
                {
                    if (!variables2.Contains(c))
                        variables2.Add(c);
                }
            }
            int[] values = new int[Math.Max(variables1.Count,variables2.Count)];
            for(int i = 0;i<values.Length;i++)
            {
                values[i] = 0;
            }
            variables1.Sort();
            variables2.Sort();
            if(variables1.Count>variables2.Count)
            {
                variables2 = variables1;
            }
            else if(variables2.Count>variables1.Count)
            {
                variables1 = variables2;
            }
            for(int i =0;i<Math.Pow(2,variables1.Count);i++)
            {
                Dictionary<char, int> valuesAndVariables = new Dictionary<char, int>();
                for(int j = 0;j<variables1.Count;j++)
                {
                    valuesAndVariables.Add(variables1[j], values[j]);
                }
                if(CountFormula(formula1,valuesAndVariables)!=CountFormula(formula2,valuesAndVariables))
                {
                    return false;
                }
                if (values[values.Length - 1] == 0)
                    values[values.Length - 1] = 1;
                else
                {
                    values[values.Length - 1] = 0;
                    for(int j = values.Length-2;j>=0;j--)
                    {
                        if (values[j] == 0)
                        {
                            values[j] = 1;
                            break;
                        }
                        else
                            values[j] = 0;
                    }
                }
            }
            return true;
        }
    }
}
