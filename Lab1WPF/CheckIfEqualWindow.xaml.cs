﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Lab1;

namespace Lab1WPF
{
    /// <summary>
    /// Interaction logic for CheckIfEqualWindow.xaml
    /// </summary>
    public partial class CheckIfEqualWindow : Window
    {
        public CheckIfEqualWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (FormulaFunction.Check(Formula1TB.Text) && (FormulaFunction.Check(Formula2TB.Text)))
            {
                if (FormulaFunction.CheckIfEqual(Formula1TB.Text, Formula2TB.Text))
                {
                    AnswerTB.Text = "Yes!";
                    AnswerTB.Background = System.Windows.Media.Brushes.Green;
                }
                else
                {
                    AnswerTB.Text = "No!";
                    AnswerTB.Background = System.Windows.Media.Brushes.Red;
                }
            }
            else if(!FormulaFunction.Check(Formula1TB.Text) && (FormulaFunction.Check(Formula2TB.Text)))
            {
                AnswerTB.Text = "Formula 1 is not a formula!";
                AnswerTB.Background = System.Windows.Media.Brushes.Red;
            }
            else if(FormulaFunction.Check(Formula1TB.Text) && (!FormulaFunction.Check(Formula2TB.Text)))
            {
                AnswerTB.Text = "Formula 2 is not a formula!";
                AnswerTB.Background = System.Windows.Media.Brushes.Red;
            }
            else
            {
                AnswerTB.Text = "Formula 1 and 2 is not a formula!";
                AnswerTB.Background = System.Windows.Media.Brushes.Red;
            }
        }
    }
}
