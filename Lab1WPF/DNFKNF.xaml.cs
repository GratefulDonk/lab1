﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Lab1;

namespace Lab1WPF
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class DnfKnfWindow : Window
    {
        public DnfKnfWindow()
        {
            InitializeComponent();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            if (FormulaFunction.Check(FormulaTB.Text))
            {
                DNFTb.Text = $"DNF:{FormulaFunction.BuildDNF(FormulaTB.Text)}";
                KNFTb.Text = $"KNF:{FormulaFunction.BuildKNF(FormulaTB.Text)}";
            }
            else
            {
                DNFTb.Text = $"NOT A FORMULA!";
                KNFTb.Text = $"NOT A FORMULA!";
                Grid b = new Grid();
                b.Background = MyGrid.Background;
                MyGrid.Background = System.Windows.Media.Brushes.Red;
                await Task.Delay(3000);
                MyGrid.Background = b.Background;
                DNFTb.Text = $"DNF:";
                KNFTb.Text = $"KNF:";
            }
        }
    }
}
